#pragma once
#include "AIController.h"
#include "Vector3.h"
#include "HierarchalModel.h"

class HumanAIController : public AIController
{
public:
	HumanAIController(HierarchalModel* model, std::vector<Vector3> movementNodes)
		:AIController(model){
		m_movementNodes = movementNodes;
		setRandomTargetPosition();
	}

	~HumanAIController();

	void update(float deltaTimeInSeconds) override {
		//Move in opposite direction if we have gone too far
		Vector3 modelPos = m_model->getPosition();
		float xDist = m_targetPos.getX() - modelPos.getX();
		float zDist = m_targetPos.getZ() - modelPos.getZ();
		float distanceFromTarget = sqrtf((xDist * xDist) + (zDist * zDist));

		//If we're close enough to target
		if (distanceFromTarget < 2){
			//Pick a random movement node to go to
			setRandomTargetPosition();
		}

		moveInDirection(xDist, zDist);

		float newX = modelPos.getX() + (deltaTimeInSeconds * m_speed * m_movementVector.getX());
		float newZ = modelPos.getZ() + (deltaTimeInSeconds * m_speed * m_movementVector.getZ());

		m_model->setPosition(Vector3(newX, modelPos.getY(), newZ));
	}

private:
	Vector3 m_targetPos;
	Vector3 m_movementVector;
	float m_speed = 10.0f;
	float const MAX_DISTANCE_FROM_MIDDLE = 30.0f;
	std::vector<Vector3> m_movementNodes;

	void setRandomTargetPosition(){
		int index = m_movementNodes.size();
		m_targetPos = m_movementNodes.at(rand() % index);
	}

	void moveInDirection(float directionInRadians){
		m_movementVector = Vector3(sin(directionInRadians), 0, cos(directionInRadians));
		m_model->setRotation(Vector3(0, directionInRadians * (180.0f / 3.14f), 0));
	}

	void moveInDirection(float xDir, float zDir){
		float directionInRadians = atan2f(xDir, zDir);
		moveInDirection(directionInRadians);
	}
};

