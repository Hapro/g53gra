#include "Cube.h"

Cube::Cube(Material material, float width, float height, float depth)
	:AnimatedDisplayableObject(material)
{
	m_width = width;
	m_height = height;
	m_depth = depth;
}

Cube::Cube(Material material, std::map<std::string, int> textures, float width, float height, float depth)
	:AnimatedDisplayableObject(material, textures)
{
	m_width = width;
	m_height = height;
	m_depth = depth;
}


Cube::~Cube()
{
}

void Cube::drawTextureMaterial(){
	glPushAttrib(GL_ALL_ATTRIB_BITS);

	setMaterialForDisplay(m_material);

	glEnable(GL_TEXTURE_2D);

	//Back
	glBindTexture(GL_TEXTURE_2D, m_textures["back"]);
	glBegin(GL_QUADS);
	glNormal3f(0, 0, -1);

	glTexCoord2f(1, 0);
	glVertex3f(0, 0, -1);

	glTexCoord2f(0, 0);
	glVertex3f(1, 0, -1);

	glTexCoord2f(0, 1);
	glVertex3f(1, 1, -1);

	glTexCoord2f(1, 1);
	glVertex3f(0, 1, -1);

	glEnd();

	//Left
	glBindTexture(GL_TEXTURE_2D, m_textures["left"]);
	glBegin(GL_QUADS);
	glNormal3f(1, 0, 0);

	glTexCoord2f(1, 0);
	glVertex3f(0, 0, 0);

	glTexCoord2f(0, 0);
	glVertex3f(0, 0, -1);

	glTexCoord2f(0, 1);
	glVertex3f(0, 1, -1);

	glTexCoord2f(1, 1);
	glVertex3f(0, 1, 0);

	glEnd();

	//Right
	glBindTexture(GL_TEXTURE_2D, m_textures["right"]);
	glBegin(GL_QUADS);
	glNormal3f(-1, 0, 0);

	glTexCoord2f(0, 1);
	glVertex3f(1, 1, 0);

	glTexCoord2f(1, 1);
	glVertex3f(1, 1, -1);

	glTexCoord2f(1, 0);
	glVertex3f(1, 0, -1);

	glTexCoord2f(0, 0);
	glVertex3f(1, 0, 0);

	glEnd();

	//Front
	glBindTexture(GL_TEXTURE_2D, m_textures["front"]);
	glBegin(GL_QUADS);

	glNormal3f(0, 0, 1);
	glTexCoord2f(0, 1);
	glVertex3f(0, 1, 0);

	glTexCoord2f(1, 1);
	glVertex3f(1, 1, 0);

	glTexCoord2f(1, 0);
	glVertex3f(1, 0, 0);

	glTexCoord2f(0, 0);
	glVertex3f(0, 0, 0);

	glEnd();

	//Top
	glBindTexture(GL_TEXTURE_2D, m_textures["top"]);
	glBegin(GL_QUADS);
	glNormal3f(0, 1, 0);

	glTexCoord2f(0, 1);
	glVertex3f(0, 1, -1);

	glTexCoord2f(1, 1);
	glVertex3f(1, 1, -1);

	glTexCoord2f(1, 0);
	glVertex3f(1, 1, 0);

	glTexCoord2f(0, 0);
	glVertex3f(0, 1, 0);

	glEnd();

	//Bottom
	glBindTexture(GL_TEXTURE_2D, m_textures["bottom"]);
	glBegin(GL_QUADS);
	glNormal3f(0, -1, 0);

	glTexCoord2f(0, 1);
	glVertex3f(0, 0, -1);

	glTexCoord2f(1, 1);
	glVertex3f(1, 0, -1);

	glTexCoord2f(1, 0);
	glVertex3f(1, 0, 0);

	glTexCoord2f(0, 0);
	glVertex3f(0, 0, 0);

	glEnd();

	glDisable(GL_TEXTURE_2D);

	glPopAttrib();
}

void Cube::drawMaterial(){
	glPushAttrib(GL_ALL_ATTRIB_BITS);

	setMaterialForDisplay(m_material);

	glBegin(GL_QUADS);

	// FRONT
	glNormal3f(0, 0, 1);
	glVertex3f(0, 0, 0);
	glVertex3f(m_width, 0, 0);
	glVertex3f(m_width, m_height, 0);
	glVertex3f(0, m_height, 0);

	// BACK
	glNormal3f(0, 0, -1);
	glVertex3f(0, 0, -m_depth);
	glVertex3f(0, m_height, -m_depth);
	glVertex3f(m_width, m_height, -m_depth);
	glVertex3f(m_width, 0, -m_depth);

	//LEFT
	glNormal3f(1, 0, 0);
	glVertex3f(0, 0, 0);
	glVertex3f(0, m_height, 0);
	glVertex3f(0, m_height, -m_depth);
	glVertex3f(0, 0, -m_depth);

	//RIGHT
	glNormal3f(-1, 0, 0);
	glVertex3f(m_width, 0, 0);
	glVertex3f(m_width, 0, -m_depth);
	glVertex3f(m_width, m_height, -m_depth);
	glVertex3f(m_width, m_height, 0);

	//TOP
	glNormal3f(0, 1, 0);
	glVertex3f(0, m_height, 0);
	glVertex3f(m_width, m_height, 0);
	glVertex3f(m_width, m_height, -m_depth);
	glVertex3f(0, m_height, -m_depth);

	//BOTTOM
	glNormal3f(0, -1, 0);
	glVertex3f(0, 0, -m_depth);
	glVertex3f(m_width, 0, -m_depth);
	glVertex3f(m_width, 0, 0);
	glVertex3f(0, 0, 0);

	glEnd();

	glPopAttrib();
}
