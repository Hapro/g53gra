#pragma once
#include "DisplayableObject.h"
#include "../AnimatedDisplayableObject.h"
#include "../Colour.h"

class Vector3Animation;

class Cube : public AnimatedDisplayableObject
{
public:
	Cube(Material material, float width, float height, float depth);
	Cube(Material material, std::map<std::string, int> textures, float width, float height, float depth);
	~Cube();

	void drawMaterial() override;
	void drawTextureMaterial() override;
	Colour m_colour;
	float m_width;
	float m_height;
	float m_depth;
};

