#include "MyScene.h"
#include "Cube.h"
#include "../DisplayTreeNode.h"
#include "../HierarchalModel.h"
#include "../Light.h"
#include "../Dog.h"
#include "../Human.h"
#include "../Skybox.h"
#include "../Building.h"
#include "../Godzilla.h"

 MyScene::MyScene(int argc, char** argv, const char *title, const int& windowWidth, const int& windowHeight)
	: Scene(argc, argv, title, windowWidth, windowHeight)
{

}

void MyScene::Initialise()
{
	//Camera starts miles away for some reason
	//so set an origin point nearby
	Vector3 origin = Vector3(0, 0, 300);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	Material lm = Material(
		Vector4(0.0f, 0.0f, 0.0f, 1.0f),
		Vector4(1.0f, 1.0f, 1.0f, 1.0f),
		Vector4(1.0f, 1.0f, 1.0f, 1.0f),
		64.0f
	);

	std::map<std::string, int> textures;
	textures.insert(std::pair<std::string, int>("left", this->GetTexture("./Textures/skybox_left.bmp")));
	textures.insert(std::pair<std::string, int>("right", this->GetTexture("./Textures/skybox_right.bmp")));
	textures.insert(std::pair<std::string, int>("top", this->GetTexture("./Textures/skybox_up.bmp")));
	textures.insert(std::pair<std::string, int>("bottom", this->GetTexture("./Textures/skybox_down.bmp")));
	textures.insert(std::pair<std::string, int>("front", this->GetTexture("./Textures/skybox_front.bmp")));
	textures.insert(std::pair<std::string, int>("back", this->GetTexture("./Textures/skybox_back.bmp")));
	textures.insert(std::pair<std::string, int>("two", this->GetTexture("./Textures/smoke.bmp")));
	textures.insert(std::pair<std::string, int>("one", this->GetTexture("./Textures/flame.bmp")));

	Skybox* skybox = new Skybox(Material(), textures);
	addModelToScene(new HierarchalModel(new DisplayTreeNode(skybox)));
	skybox->position(origin.getX() - 100, 0, origin.getZ() + 100);

	std::vector<Vector3> targets;

	for (int x = 0; x < 3; x++){
		for (int z = 0; z < 3; z++){
			Building* building = new Building(Colour(0.3f, 0.3f, 0.3f), (rand() % 6) + 2);
			float xPos = (x * 50) + origin.getX();
			float zPos = (z * 50) + origin.getZ() - 100;
			building->setPosition(Vector3(xPos, 0, zPos));
			addModelToScene(building);

			targets.push_back(Vector3(xPos + 25, 0, zPos + 25));
		}
	}

	for (int i = 0; i < 5; i++){
		Human* h = new Human();
		h->setPosition(Vector3(origin.getX() + (i * 50), origin.getY() + 2, origin.getY() + (i * 50)));
		addModelToScene(h);
		m_aiControllers.push_back(new HumanAIController(h, targets));
	}

	Godzilla* godzilla = new Godzilla(textures);
	godzilla->setPosition(Vector3(origin.getX() + 25, origin.getY(), origin.getZ()));
	addModelToScene(godzilla);
	m_aiControllers.push_back(new HumanAIController(godzilla, targets));

	addLight(new Light(lm, GL_LIGHT0, 0, 50, -10));
}

void MyScene::Projection()
{
	GLdouble aspect = static_cast<GLdouble>(windowWidth) / static_cast<GLdouble>(windowHeight);
	gluPerspective(60.0, aspect, 1.0, 1000.0);
}
