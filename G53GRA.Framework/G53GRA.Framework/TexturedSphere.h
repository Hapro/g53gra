#pragma once

#include "AnimatedDisplayableObject.h"

#include <string>
#include <map>

// Pointer index for vertex 0
#define _VERTEX0 0
// Pointer index for vertex 1
#define _VERTEX1 3
// Pointer index for vertex 2
#define _VERTEX2 6
// Pointer index for vertex 3
#define _VERTEX3 9
// Pointer index for vertex 4
#define _VERTEX4 12
// Pointer index for vertex 5
#define _VERTEX5 15
// Cap subdivisons at 7
#define _MAX_RES 7

class TexturedSphere : public AnimatedDisplayableObject
{
public:
	TexturedSphere();
	TexturedSphere(Material material, std::map<std::string, int> textures, int res);
	~TexturedSphere(){}

	// Manually set resolution
	inline void SetResolution(int r) { _resolution = r >= 0 ? (r > _MAX_RES ? _MAX_RES : r) : 0; }

	void drawTextureMaterial() override;
	void drawMaterial() override {};

protected:
	void DrawSphere();
	void SubDivide(int p_recurse, float *a, float *b, float *c);
	void DrawFace(float *a, float *b, float *c);
	void FixSeam(float &a, float &b, float &c);

	// ID to bound texture
	int _texID;
	int _texID2;
	// recursive resolution (increase for finer mesh)
	int _resolution;
	// Initial vertices
	float* _initV;
};

