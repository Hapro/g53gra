#include "Godzilla.h"
#include "DisplayTreeNode.h"
#include "DisplayableObject.h"
#include "Cube.h"
#include "Material.h"
#include "Vector4.h"
#include "Tetrahedron.h"
#include "TexturedSphere.h"

Godzilla::Godzilla(std::map<std::string, int> textures)
{
	Material m = Materials::shiny(Colours::Pink());

	TexturedSphere* flame = new TexturedSphere(Materials::shiny(Colours::Red()), textures, 3);
	Cube* head = new Cube(Materials::shiny(Colours::Green()), 2, 1, 2);
	Cube* mouth = new Cube(Materials::shiny(Colours::Green()), 2, 0.5f, 1);
	Tetrahedron* body = new Tetrahedron(Materials::shiny(Colours::Green()), 4, 6, 4);
	Tetrahedron* bodyBack = new Tetrahedron(Materials::shiny(Colours::Green()), 3, 3, 3);
	Cube* leftArm = new Cube(Materials::shiny(Colours::Green()), 1, 1, 2);
	Cube* rightArm = new Cube(Materials::shiny(Colours::Green()), 1, 1, 2);
	Cube* leftLeg = new Cube(Materials::shiny(Colours::Green()), 1, 1, 2);
	Cube* rightLeg = new Cube(Materials::shiny(Colours::Green()), 1, 1, 2);

	body->position(0, 0, 0);
	head->position(1, 5, 1);
	mouth->position(0, -1, 0);
	bodyBack->position(0.5f, 0, -3);
	leftArm->position(-0.5f, 2.5f, 1);
	rightArm->position(3.5f, 2.5f, 1);
	leftLeg->position(-1, 0, 0);
	rightLeg->position(4, 0, 0);
	flame->position(0.5f, 0, 0.5f);
	flame->size(0.5f);

	flame->getPositionAnimation()->addAnimationFrame(0, 0, 0);
	flame->getPositionAnimation()->addAnimationFrame(0, 0, 5);

	mouth->getRotationAnimation()->addAnimationFrame(25, 0, 0);
	mouth->getRotationAnimation()->addAnimationFrame(0, 0, 0);
	mouth->setRotationOrigin(1.0f, 0.0f, -1.0f);

	rightArm->getRotationAnimation()->addAnimationFrame(0, 30, 0);
	rightArm->getRotationAnimation()->addAnimationFrame(0, 0, 0);
	rightArm->setRotationOrigin(1.0f, 0.0f, -2.0f);

	leftArm->getRotationAnimation()->addAnimationFrame(0, 0, 0);
	leftArm->getRotationAnimation()->addAnimationFrame(0, -30, 0);
	leftArm->setRotationOrigin(1.0f, 0.0f, -2.0f);

	leftLeg->getRotationAnimation()->addAnimationFrame(25, 0, 0);
	leftLeg->getRotationAnimation()->addAnimationFrame(-25, 0, 0);
	leftLeg->setRotationOrigin(0.0f, 2.0f, -0.5f);

	rightLeg->getRotationAnimation()->addAnimationFrame(-25, 0, 0);
	rightLeg->getRotationAnimation()->addAnimationFrame(25, 0, 0);
	rightLeg->setRotationOrigin(0.0f, 2.0f, -0.5f);

	DisplayTreeNode* headNode = new DisplayTreeNode(head);
	DisplayTreeNode* bodyBackNode = new DisplayTreeNode(bodyBack);
	DisplayTreeNode* bodyNode = new DisplayTreeNode(body);
	DisplayTreeNode* rightArmNode = new DisplayTreeNode(rightArm);
	DisplayTreeNode* leftArmNode = new DisplayTreeNode(leftArm);
	DisplayTreeNode* leftLegNode = new DisplayTreeNode(leftLeg);
	DisplayTreeNode* rightLegNode = new DisplayTreeNode(rightLeg);
	DisplayTreeNode* mouthNode = new DisplayTreeNode(mouth);
	DisplayTreeNode* flameNode = new DisplayTreeNode(flame);

	bodyNode->addChild(headNode);
	headNode->addChild(mouthNode);
	bodyNode->addChild(bodyBackNode);
	bodyNode->addChild(leftArmNode);
	bodyNode->addChild(rightArmNode);
	bodyNode->addChild(leftLegNode);
	bodyNode->addChild(rightLegNode);
	headNode->addChild(flameNode);

	bodyNode->getDisplayableObject()->size(5.0f);
	this->setParentDisplayTreeNode(bodyNode);
}


Godzilla::~Godzilla()
{
}
