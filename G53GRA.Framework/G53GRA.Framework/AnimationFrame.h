#pragma once
class AnimationFrame
{
public:
	AnimationFrame(float value){
		m_value = value;
	}
	~AnimationFrame() {}

	float getValue() { return m_value;  }

private:
	float m_value;
};

