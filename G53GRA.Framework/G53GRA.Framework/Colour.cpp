#include "Colour.h"

Colour::Colour()
{
	m_red = 0;
	m_green = 0;
	m_blue = 0;
}

Colour::Colour(float red, float green, float blue)
{
	m_red = red;
	m_green = green;
	m_blue = blue;
}

Colour::~Colour()
{
}

float Colour::red()
{
	return m_red;
}

float Colour::green()
{
	return m_green;
}

float Colour::blue()
{
	return m_blue;
}
