#include "HierarchalModel.h"
#include "../DisplayTreeNode.h"
#include "DisplayableObject.h"
#include "AnimatedDisplayableObject.h"

HierarchalModel::HierarchalModel(DisplayTreeNode * pDisplayTreeNode)
{
	m_pParentDisplayTreeNode = pDisplayTreeNode;
}

HierarchalModel::HierarchalModel()
{
}

HierarchalModel::~HierarchalModel()
{
}

DisplayTreeNode * HierarchalModel::getParentDisplayTreeNode()
{
	return m_pParentDisplayTreeNode;
}


std::vector<DisplayableObject*> HierarchalModel::getBreadthFirstOrder()
{
	std::vector<DisplayableObject*> lst;

	HierarchalModel::iterateThroughDisplayableObjects([&](DisplayableObject* obj) {
		lst.push_back(obj);
	});

	return lst;
}

void HierarchalModel::display() {
	display(m_pParentDisplayTreeNode);
}

void HierarchalModel::update(float deltaTime)
{
	AnimatedDisplayableObject* animatedObj;

	HierarchalModel::iterateThroughDisplayableObjects([&](DisplayableObject* obj) {
		animatedObj = static_cast<AnimatedDisplayableObject*>(obj);

		if (animatedObj != NULL) {
			animatedObj->update(deltaTime);
		}
	});
}

void HierarchalModel::display(DisplayTreeNode* node) {
	if (node->getHasObject()) {
		node->getDisplayableObject()->preDisplay();
		node->getDisplayableObject()->Display();
	}

	for (int i = 0; i < node->getChildren().size(); i++) {
		display(node->getChildren().at(i));
	}

	if (node->getHasObject()) {
		node->getDisplayableObject()->postDisplay();
	}
}

void HierarchalModel::setParentDisplayTreeNode(DisplayTreeNode * pParentDisplayTreeNode)
{
	m_pParentDisplayTreeNode = pParentDisplayTreeNode;
}

void HierarchalModel::setPosition(Vector3 position){
	if (m_pParentDisplayTreeNode != NULL){
		if (m_pParentDisplayTreeNode->getHasObject()){
			//Let's just assume we can mess with these addresses for now
			float* pos = m_pParentDisplayTreeNode->getDisplayableObject()->position();

			pos[0] = position.getX();
			pos[1] = position.getY();
			pos[2] = position.getZ();
		}
	}
}

Vector3 HierarchalModel::getPosition(){
	if (m_pParentDisplayTreeNode != NULL){
		if (m_pParentDisplayTreeNode->getHasObject()){
			//Let's just assume we can mess with these addresses for now
			float* pos = m_pParentDisplayTreeNode->getDisplayableObject()->position();

			return Vector3(pos[0], pos[1], pos[2]);
		}
	}

	return Vector3(0, 0, 0);
}

Vector3 HierarchalModel::getRotation(){
	if (m_pParentDisplayTreeNode != NULL){
		if (m_pParentDisplayTreeNode->getHasObject()){
			//Let's just assume we can mess with these addresses for now
			float* rot = m_pParentDisplayTreeNode->getDisplayableObject()->orientation();

			return Vector3(rot[0], rot[1], rot[2]);
		}
	}
}

void HierarchalModel::setRotation(Vector3 rotation){
	if (m_pParentDisplayTreeNode != NULL){
		if (m_pParentDisplayTreeNode->getHasObject()){
			//Let's just assume we can mess with these addresses for now
			float* rot = m_pParentDisplayTreeNode->getDisplayableObject()->orientation();

			rot[0] = rotation.getX();
			rot[1] = rotation.getY();
			rot[2] = rotation.getZ();
		}
	}
}
