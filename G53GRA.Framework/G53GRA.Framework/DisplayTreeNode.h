#pragma once
#include <vector>

class DisplayableObject;

class DisplayTreeNode
{
public:
	DisplayTreeNode();
	DisplayTreeNode(DisplayableObject* pDisplayableObject);
	~DisplayTreeNode();

	DisplayableObject* getDisplayableObject();
	void addChild(DisplayTreeNode* pDisplayTreeNode);
	std::vector<DisplayTreeNode*> getChildren();
	bool getHasObject();

private:
	DisplayableObject* g_pDisplayableObject;
	std::vector<DisplayTreeNode*> g_children;
	bool m_hasObject;
};
