#pragma once
#include "DisplayableObject.h"
#include "../AnimatedDisplayableObject.h"
#include "../Colour.h"

class Vector3Animation;

class Tetrahedron : public AnimatedDisplayableObject
{
public:
	Tetrahedron(Material material, float width, float height, float depth);
	Tetrahedron(Material material, std::map<std::string, int> textures, float width, float height, float depth);
	~Tetrahedron();

	void drawMaterial() override;
	void drawTextureMaterial() override;
	Colour m_colour;
	float m_width;
	float m_height;
	float m_depth;
};

