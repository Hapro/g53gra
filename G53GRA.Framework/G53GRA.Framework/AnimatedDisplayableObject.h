#pragma once
#include "DisplayableObject.h"
#include "Vector3Animation.h"

class AnimatedDisplayableObject :
	public DisplayableObject
{
public:
	AnimatedDisplayableObject(Material material) 
		:DisplayableObject(material){
		m_positionAnimation = new Vector3Animation();
		m_rotationAnimation = new Vector3Animation();
		m_rotationOrigin = Vector4(0, 0, 0, 0);
	}

	AnimatedDisplayableObject(Material material, std::map<std::string, int> textures)
		:DisplayableObject(material, textures){
		m_positionAnimation = new Vector3Animation();
		m_rotationAnimation = new Vector3Animation();
		m_rotationOrigin = Vector4(0, 0, 0, 0);
	}

	~AnimatedDisplayableObject() {}

	void update(float deltaTimeInSeconds) {
		m_positionAnimation->update(deltaTimeInSeconds);
		m_rotationAnimation->update(deltaTimeInSeconds);
	}

	void Display() override {
		glTranslatef(
			pos[0] + m_positionAnimation->getXAnimation()->getValue(),
			pos[1] + m_positionAnimation->getYAnimation()->getValue(),
			pos[2] + m_positionAnimation->getZAnimation()->getValue());
		glScalef(scale[0], scale[1], scale[2]);

		//Rotate around origin
		glTranslatef(m_rotationOrigin.getX(), m_rotationOrigin.getY(), m_rotationOrigin.getZ());

		glRotatef(rotation[1] + m_rotationAnimation->getYAnimation()->getValue(), 0.0f, 1.0f, 0.0f);
		glRotatef(rotation[2] + m_rotationAnimation->getZAnimation()->getValue(), 0.0f, 0.0f, 1.0f);
		glRotatef(rotation[0] + m_rotationAnimation->getXAnimation()->getValue(), 1.0f, 0.0f, 0.0f);

		glTranslatef(-m_rotationOrigin.getX(), -m_rotationOrigin.getY(), -m_rotationOrigin.getZ());

		switch (m_displayType){
		case MATERIAL:
			drawMaterial();
			break;

		case TEXTURE_MATERIAL:
			drawTextureMaterial();
			break;
		}
	} 

	Vector3Animation* getPositionAnimation(){
		return m_positionAnimation;
	}

	Vector3Animation* getRotationAnimation(){
		return m_rotationAnimation;
	}

	void setRotationOrigin(float x, float y, float z){
		m_rotationOrigin = Vector4(x, y, z, 0);
	}

private:
	Vector3Animation* m_positionAnimation;
	Vector3Animation* m_rotationAnimation;
	Vector4 m_rotationOrigin;
};

