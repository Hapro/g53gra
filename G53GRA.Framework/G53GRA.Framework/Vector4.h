#pragma once
class Vector4
{
public:
	Vector4();
	Vector4(float x, float y, float z, float a);
	~Vector4();

	float getX();
	float getY();
	float getZ();
	float getA();
	float* toFloatArray();

private:
	float m_x;
	float m_y;
	float m_z;
	float m_a;
};

