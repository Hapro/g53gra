#pragma once
class HierarchalModel;

class AIController
{
public:
	AIController(HierarchalModel* model){ m_model = model; }
	~AIController(){}
	virtual void update(float deltaTimeInSeconds){}

protected:
	HierarchalModel* m_model;
};

