#include "Human.h"
#include "DisplayTreeNode.h"
#include "DisplayableObject.h"
#include "Cube.h"
#include "Material.h"
#include "Vector4.h"

Human::Human()
{
	Material m = Materials::shiny(Colours::Pink());

	Cube* head = new Cube(Materials::shiny(Colours::Pink()), 2, 2, 2);
	Cube* body = new Cube(Materials::shiny(Colours::Blue()), 1, 3, 1);
	Cube* leftArm = new Cube(Materials::shiny(Colours::Green()), 2, 0.5f, 1);
	Cube* rightArm = new Cube(Materials::shiny(Colours::Red()), 2, 0.5f, 1);
	Cube* leftLeg = new Cube(Materials::shiny(Colours::Brown()), 0.3f, 2, 1);
	Cube* rightLeg = new Cube(Materials::shiny(Colours::Teal()), 0.3f, 2, 1);

	body->position(0, 0, 0);
	head->position(-0.5f, 3, 0.5f);
	leftArm->position(-2, 2, 0);
	rightArm->position(1, 2, 0);
	leftLeg->position(-0.3f, -2, 0);
	rightLeg->position(1, -2, 0);

	rightArm->getRotationAnimation()->addAnimationFrame(0, -30, -45);
	rightArm->getRotationAnimation()->addAnimationFrame(0, 30, -45);
	rightArm->setRotationOrigin(0.0f, 0.0f, 0.0f);

	leftArm->getRotationAnimation()->addAnimationFrame(0, -30, 45);
	leftArm->getRotationAnimation()->addAnimationFrame(0, 30, 45);
	leftArm->setRotationOrigin(2.0f, 0.0f, 0.0f);

	leftLeg->getRotationAnimation()->addAnimationFrame(25, 0, 0);
	leftLeg->getRotationAnimation()->addAnimationFrame(-25, 0, 0);
	leftLeg->setRotationOrigin(0.0f, 2.0f, -0.5f);

	rightLeg->getRotationAnimation()->addAnimationFrame(-25, 0, 0);
	rightLeg->getRotationAnimation()->addAnimationFrame(25, 0, 0);
	rightLeg->setRotationOrigin(0.0f, 2.0f, -0.5f);

	DisplayTreeNode* headNode = new DisplayTreeNode(head);
	DisplayTreeNode* bodyNode = new DisplayTreeNode(body);
	DisplayTreeNode* rightArmNode = new DisplayTreeNode(rightArm);
	DisplayTreeNode* leftArmNode = new DisplayTreeNode(leftArm);
	DisplayTreeNode* leftLegNode = new DisplayTreeNode(leftLeg);
	DisplayTreeNode* rightLegNode = new DisplayTreeNode(rightLeg);

	bodyNode->addChild(headNode);
	bodyNode->addChild(leftArmNode);
	bodyNode->addChild(rightArmNode);
	bodyNode->addChild(leftLegNode);
	bodyNode->addChild(rightLegNode);

	this->setParentDisplayTreeNode(bodyNode);
}


Human::~Human()
{
}
