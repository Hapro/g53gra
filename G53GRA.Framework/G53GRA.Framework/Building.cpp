#include "Building.h"
#include "Cube.h"
#include "DisplayTreeNode.h"

Building::Building(Colour colour, int stories)
{
	DisplayTreeNode* base = new DisplayTreeNode(new Cube(Materials::shiny(colour), SIZE, SIZE, SIZE));
	this->setParentDisplayTreeNode(base);

	DisplayTreeNode* door = NULL;

	//Create a doow on a side
	switch (rand() % 4){
	case 0: //Left
		door = new DisplayTreeNode(new Cube(Materials::shiny(Colour(1.0f, 0.0f, 0.0f)), WINDOW_SIZE, WINDOW_SIZE * 2, WINDOW_SIZE));
		door->getDisplayableObject()->position(-(WINDOW_SIZE / 2), 0, -((SIZE / 2) - (WINDOW_SIZE / 2)));
		break;

	case 1: //Right
		door = new DisplayTreeNode(new Cube(Materials::shiny(Colour(1.0f, 0.0f, 0.0f)), WINDOW_SIZE, WINDOW_SIZE, WINDOW_SIZE));
		door->getDisplayableObject()->position(SIZE - (WINDOW_SIZE / 2), 0, -((SIZE / 2) - (WINDOW_SIZE / 2)));
		break;

	case 2: //Front
		door = new DisplayTreeNode(new Cube(Materials::shiny(Colour(1.0f, 0.0f, 0.0f)), WINDOW_SIZE, WINDOW_SIZE, WINDOW_SIZE));
		door->getDisplayableObject()->position(((SIZE / 2) - (WINDOW_SIZE / 2)), 0, (WINDOW_SIZE / 2));
		break;

	case 3:
		door = new DisplayTreeNode(new Cube(Materials::shiny(Colour(1.0f, 0.0f, 0.0f)), WINDOW_SIZE, WINDOW_SIZE, WINDOW_SIZE));
		door->getDisplayableObject()->position(((SIZE / 2) - (WINDOW_SIZE / 2)), 0, -(SIZE - (WINDOW_SIZE / 2)));
		break;
	}

	base->addChild(door);

	DisplayTreeNode* n = base;

	for (int i = 1; i < stories+1; i++){
		Colour windowColour = Colour(0.0f, 1.0f, 1.0f);
		DisplayTreeNode* buildingSegment = new DisplayTreeNode(new Cube(Materials::shiny(colour), SIZE, SIZE, SIZE));

		DisplayTreeNode* window1 = new DisplayTreeNode(new Cube(Materials::shiny(windowColour), WINDOW_SIZE, WINDOW_SIZE, WINDOW_SIZE));
		window1->getDisplayableObject()->position(-(WINDOW_SIZE / 3), 0, -((SIZE / 2) - (WINDOW_SIZE / 3)));
			
		DisplayTreeNode* window2 = new DisplayTreeNode(new Cube(Materials::shiny(windowColour), WINDOW_SIZE, WINDOW_SIZE, WINDOW_SIZE));
		window2->getDisplayableObject()->position(SIZE - (WINDOW_SIZE / 3), 0, -((SIZE / 2) - (WINDOW_SIZE / 3)));

		DisplayTreeNode* window3 = new DisplayTreeNode(new Cube(Materials::shiny(windowColour), WINDOW_SIZE, WINDOW_SIZE, WINDOW_SIZE));
		window3->getDisplayableObject()->position(((SIZE / 2) - (WINDOW_SIZE / 3)), 0, (WINDOW_SIZE / 3));

		DisplayTreeNode* window4 = new DisplayTreeNode(new Cube(Materials::shiny(windowColour), WINDOW_SIZE, WINDOW_SIZE, WINDOW_SIZE));
		window4->getDisplayableObject()->position(((SIZE / 2) - (WINDOW_SIZE / 3)), 0, -(SIZE - (WINDOW_SIZE / 3)));

		buildingSegment->addChild(window1);
		buildingSegment->addChild(window2);
		buildingSegment->addChild(window3);
		buildingSegment->addChild(window4);

		buildingSegment->getDisplayableObject()->position(0, SIZE, 0);
		n->addChild(buildingSegment);
		n = buildingSegment;
	}
}


Building::~Building()
{
}
