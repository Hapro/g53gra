#pragma once
#include <vector>
#include <list>
#include "../Vector3.h"

class DisplayTreeNode;
class DisplayableObject;

class HierarchalModel
{
public:
	HierarchalModel(DisplayTreeNode* pDisplayTreeNode);
	HierarchalModel();
	~HierarchalModel();

	DisplayTreeNode* getParentDisplayTreeNode();
	std::vector<DisplayableObject*> getBreadthFirstOrder();

	template<typename Func>
	inline void iterateThroughDisplayableObjects(Func func) {
		DisplayTreeNode* n;
		std::list<DisplayTreeNode*> lst;

		lst.push_back(m_pParentDisplayTreeNode);

		do {
			n = lst.front();
			lst.pop_front();

			for (int i = 0; i < n->getChildren().size(); i++) {
				DisplayTreeNode* c = n->getChildren().at(i);
				lst.push_back(c);

				if(c->getHasObject()) func(c->getDisplayableObject());
			}

		} while (lst.size() > 0);
	}

	void display();
	virtual void update(float deltaTime);
	void setPosition(Vector3 position);
	Vector3 getPosition();
	Vector3 getRotation();
	void setRotation(Vector3 rotation);

private:
	DisplayTreeNode* m_pParentDisplayTreeNode;
	void display(DisplayTreeNode* node);

protected:
	void setParentDisplayTreeNode(DisplayTreeNode* pParentDisplayTreeNode);
};



