#pragma once
#include <vector>
#include "AnimationFrame.h"
class Animation
{
public:
	Animation(float durationInSeconds) {
		m_durationInSeconds = durationInSeconds;
		m_currentFrame = 0;
	}

	~Animation() {
		for (AnimationFrame* f : m_animationFrames) f->~AnimationFrame();
	}

	void addFrame(AnimationFrame* frame) {
		m_animationFrames.push_back(frame);
	}

	void update(float deltaTimeSeconds) {
		m_timeElapsedInSeconds += deltaTimeSeconds;

		if (m_timeElapsedInSeconds > getAnimationLength()) {
			m_timeElapsedInSeconds = 0;
			m_currentFrame++;
		}

		if (m_currentFrame >= m_animationFrames.size()){
			m_currentFrame = 0;
		}
	}

	int getAnimationLength() {
		return m_animationFrames.size() * TIME_BETWEEN_FRAMES_IN_SECONDS;
	}

	float getValue() {

		if (m_animationFrames.size() == 0) return 0.0f;

		AnimationFrame* currentFrame;
		AnimationFrame* nextFrame;

		currentFrame = m_animationFrames.at(m_currentFrame);

		if (m_currentFrame == m_animationFrames.size() - 1){
			nextFrame = m_animationFrames.at(0);
		}
		else{
			nextFrame = m_animationFrames.at(m_currentFrame + 1);
		}

		float percentage = m_timeElapsedInSeconds / TIME_BETWEEN_FRAMES_IN_SECONDS;

		if (percentage > 1.0f) percentage = 1.0f;
		if (percentage < 0) percentage = 0.0f;

		//Lerp between the two
		float value = lerp(currentFrame->getValue(), nextFrame->getValue(), percentage);

		if (percentage > 1.0f){
			std::printf("%f\n", percentage);
		}

		return value;
	}

	void setDuration(float duration){
		m_durationInSeconds = duration;
	}

	float lerp(float y1, float y2,float mu){
		return(y1*(1 - mu) + y2*mu);
	}

private:
	std::vector<AnimationFrame*> m_animationFrames;
	float m_durationInSeconds;
	float m_timeElapsedInSeconds;
	static const int TIME_BETWEEN_FRAMES_IN_SECONDS = 2;
	int m_currentFrame;
};

