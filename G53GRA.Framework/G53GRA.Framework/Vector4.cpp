#include "Vector4.h"

Vector4::Vector4()
{
	m_x = 0;
	m_y = 0;
	m_z = 0;
	m_a = 0;
}


Vector4::Vector4(float x, float y, float z, float a)
{
	m_x = x;
	m_y = y;
	m_z = z;
	m_a = a;
}

Vector4::~Vector4()
{
}

float Vector4::getX()
{
	return m_x;
}

float Vector4::getY()
{
	return m_y;
}

float Vector4::getZ()
{
	return m_z;
}

float Vector4::getA()
{
	return m_a;
}

float* Vector4::toFloatArray()
{
	float arr[4] = { m_x, m_y, m_z, m_a };
	return arr;
}
