#pragma once
#include "Material.h"

class Light
{
public:
	Light(Material material, long glLightNumber, float x, float y, float z);
	~Light();

	void setX(float x);
	void setY(float y);
	void setZ(float z);
	Vector4 getPosition();
	void update();

private:
	Material m_material;
	Vector4 m_position;
	long m_glLightNumber;
};

