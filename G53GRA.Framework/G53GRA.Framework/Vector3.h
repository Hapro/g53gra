#pragma once
class Vector3
{
public:
	Vector3(){
		m_x = 0;
		m_y = 0;
		m_z = 0;
	}

	Vector3(float x, float y, float z){
		m_x = x;
		m_y = y;
		m_z = z;
	}

	float getX(){ return m_x; }
	float getY(){ return m_y; }
	float getZ(){ return m_z; }

	~Vector3(){}

private:
	float m_x;
	float m_y;
	float m_z;
};

