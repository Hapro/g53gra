#pragma once

#include "Scene.h"
#include <vector>
#include "../Material.h"
#include <map>

/**
* Virtual class to be inherited by all objects to be displayed in Scene
* <p>
* Contains purely virtual {@link #Display} method that must be overloaded. {@link #Display()} is called from {@link Scene}.
* @author wil
*/
class DisplayableObject
{
public:
	enum DisplayType{
		MATERIAL,
		TEXTURE_MATERIAL
	};

	DisplayableObject() {
		m_material = Material();
		position(0.0f, 0.0f, 0.0f);
		size(1.0f);
		orientation(0.0f, 0.0f, 0.0f);
		m_displayType = MATERIAL;
		m_colour = Colour(1, 1, 1);
	}

	DisplayableObject(Material material){
		m_material = material;
		position(0.0f, 0.0f, 0.0f);
		size(1.0f);
		orientation(0.0f, 0.0f, 0.0f);
		m_displayType = MATERIAL;
	};

	DisplayableObject(Material material, std::map<std::string, int> textures){
		m_material = material;
		position(0.0f, 0.0f, 0.0f);
		size(1.0f);
		orientation(0.0f, 0.0f, 0.0f);
		m_displayType = TEXTURE_MATERIAL;
		m_textures = textures;
	};

	virtual ~DisplayableObject(void){};
	
	void preDisplay() {
		glPushMatrix();
	}

	void postDisplay() {
		glPopMatrix();
	}

	/**
	* Virtual method. Called from Scene parent.
	* <p>
	* Must be overloaded by your DisplayableObject subclass. Contains all rendering commands.
	*/
	virtual void Display() {		
		glTranslatef(pos[0], pos[1], pos[2]);
		glScalef(scale[0], scale[1], scale[2]);

		glRotatef(rotation[1], 0.0f, 1.0f, 0.0f);
		glRotatef(rotation[2], 0.0f, 0.0f, 1.0f);
		glRotatef(rotation[0], 1.0f, 0.0f, 0.0f);
		
		switch (m_displayType){
			case MATERIAL:
				drawMaterial();
				break;

			case TEXTURE_MATERIAL:
				drawTextureMaterial();
				break;
		}
	}

	/**
	* Override with code to draw the object
	*/
	virtual void drawMaterial() = 0;

	virtual void drawTextureMaterial() = 0;

	/** set World Space position */
	void position(float x, float y, float z){
		pos[0] = x;
		pos[1] = y;
		pos[2] = z;
	}
	
	/** set size in World Space (single value) */
	void size(float s){
		size(s, s, s);
	}

/** set size in World Space (seperate axes) */
	void size(float sx, float sy, float sz){
		scale[0] = sx;
		scale[1] = sy;
		scale[2] = sz;
	}

	/** set orientation in World Space */
	void orientation(float rx, float ry, float rz){
		rotation[0] = rx;
		rotation[1] = ry;
		rotation[2] = rz;
	}

	/** Get size in World Space */
	float* size(){
		return scale;
	}

/** Get orientation in World Space */
	float* orientation(){
		return rotation;
	}

/** Get World Space position */
	float* position(){
		return pos;
	}

protected:
	/** float[] containing World Space position coordinates */
	float pos[3];
	/** float[] containing relative World Space scaling values for x,y,z */
	float scale[3];
	/** float[] containing angles of orientation in World Space for x, y, and z axes */
	float rotation[3];

	Material m_material;
	Colour m_colour;
	std::map<std::string, int> m_textures;
	DisplayType m_displayType;

	// Call glPushAttrib(GL_ALL_ATTRIB_BITS); before, and glPopAttrib() after
	void setMaterialForDisplay(Material material){
		Vector4 specular = material.getSpecular();
		Vector4 diffuse = material.getDiffuse();
		Vector4 ambient = material.getAmbient();

		float diffuse_colour[] = { diffuse.getX(), diffuse.getY(), diffuse.getZ(), diffuse.getA() };
		float ambient_colour[] = { ambient.getX(), ambient.getY(), ambient.getZ(), ambient.getA() };
		float specular_colour[] = { specular.getX(), specular.getY(), specular.getZ(), specular.getA() };

		glMaterialfv(GL_FRONT, GL_AMBIENT, ambient_colour);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse_colour);
		glMaterialfv(GL_FRONT, GL_SPECULAR, specular_colour);
		glMaterialf(GL_FRONT, GL_SHININESS, material.getShine());
	}
};
