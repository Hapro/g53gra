#include "DisplayTreeNode.h"

DisplayTreeNode::DisplayTreeNode() {
	m_hasObject = false;
}

DisplayTreeNode::DisplayTreeNode(DisplayableObject* pDisplayableObject)
{
	g_pDisplayableObject = pDisplayableObject;
	g_children = std::vector<DisplayTreeNode*>();

	m_hasObject = g_pDisplayableObject != NULL;
}


DisplayTreeNode::~DisplayTreeNode()
{
}

DisplayableObject* DisplayTreeNode::getDisplayableObject() {
	return g_pDisplayableObject;
}

void DisplayTreeNode::addChild(DisplayTreeNode* pDisplayTreeNode) {
	g_children.push_back(pDisplayTreeNode);
}

std::vector<DisplayTreeNode*> DisplayTreeNode::getChildren()
{
	return g_children;
}

bool DisplayTreeNode::getHasObject() {
	return m_hasObject;
}
