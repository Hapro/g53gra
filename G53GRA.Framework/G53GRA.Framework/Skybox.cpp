#include "Skybox.h"

Skybox::Skybox(Material material, std::map<std::string, int> textures)
	:DisplayableObject(material, textures)
{
	this->scale[0] = SIZE;
	this->scale[1] = SIZE / 2;
	this->scale[2] = SIZE;
}


Skybox::~Skybox()
{
}

void Skybox::drawTextureMaterial(){
	//glDisable(GL_LIGHTING);                 // Disable lighting just for skybox
	//glColor4f(1.f, 1.f, 1.f, 0.f);          // Set fill to be invisible (only texture is rendered)
	glEnable(GL_TEXTURE_2D);

	//Back
	glBindTexture(GL_TEXTURE_2D, m_textures["back"]);
	glBegin(GL_QUADS);

	glTexCoord2f(1, 0);
	glVertex3f(0, 0, -1);

	glTexCoord2f(0, 0);
	glVertex3f(1, 0, -1);

	glTexCoord2f(0, 1);
	glVertex3f(1, 1, -1);

	glTexCoord2f(1, 1);
	glVertex3f(0, 1, -1);

	glEnd();

	//Left
	glBindTexture(GL_TEXTURE_2D, m_textures["left"]);
	glBegin(GL_QUADS);

	glTexCoord2f(1, 0);
	glVertex3f(0, 0, 0);

	glTexCoord2f(0, 0);
	glVertex3f(0, 0, -1);

	glTexCoord2f(0, 1);
	glVertex3f(0, 1, -1);

	glTexCoord2f(1, 1);
	glVertex3f(0, 1, 0);

	glEnd();

	//Right
	glBindTexture(GL_TEXTURE_2D, m_textures["right"]);
	glBegin(GL_QUADS);
	
	glTexCoord2f(0, 1);
	glVertex3f(1, 1, 0);
	
	glTexCoord2f(1, 1);
	glVertex3f(1, 1, -1);

	glTexCoord2f(1, 0);
	glVertex3f(1, 0, -1);

	glTexCoord2f(0, 0);
	glVertex3f(1, 0, 0);

	glEnd();

	//Front
	glBindTexture(GL_TEXTURE_2D, m_textures["front"]);
	glBegin(GL_QUADS);
	
	glTexCoord2f(0, 1);
	glVertex3f(0, 1, 0);

	glTexCoord2f(1, 1);
	glVertex3f(1, 1, 0);

	glTexCoord2f(1, 0);
	glVertex3f(1, 0, 0);

	glTexCoord2f(0, 0);
	glVertex3f(0, 0, 0);

	glEnd();

	//Top
	glBindTexture(GL_TEXTURE_2D, m_textures["top"]);
	glBegin(GL_QUADS);

	glTexCoord2f(0, 1);
	glVertex3f(0, 1, -1);

	glTexCoord2f(1, 1);
	glVertex3f(1, 1, -1);

	glTexCoord2f(1, 0);
	glVertex3f(1, 1, 0);

	glTexCoord2f(0, 0);
	glVertex3f(0, 1, 0);

	glEnd();

	//Bottom
	glBindTexture(GL_TEXTURE_2D, m_textures["bottom"]);
	glBegin(GL_QUADS);

	glTexCoord2f(0, 0);
	glVertex3f(0, 0, 0);

	glTexCoord2f(1, 0);
	glVertex3f(1, 0, 0);

	glTexCoord2f(1, 1);
	glVertex3f(1, 0, -1);

	glTexCoord2f(0, 1);
	glVertex3f(0, 0, -1);

	glEnd();

	glDisable(GL_TEXTURE_2D);
}