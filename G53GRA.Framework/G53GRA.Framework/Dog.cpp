#include "Dog.h"
#include "DisplayTreeNode.h"
#include "DisplayableObject.h"
#include "Cube.h"
#include "Material.h"
#include "Vector4.h"

Dog::Dog()
	:HierarchalModel()
{
	Material m = Material(
		Vector4(1.0f, 1.0f, 1.0f, 1.0f),
		Vector4(1.0f, 1.0f, 1.0f, 1.0f),
		Vector4(1.0f, 1.0f, 1.0f, 1.0f),
		64.0f);

	Cube* head = new Cube(m, 1, 1, 1);
	Cube* body = new Cube(m, 3, 1, 1);
	Cube* tail = new Cube(m, 3, 0.3f, 0.3f);

	head->position(0, 0, 0);
	body->position(1, -1, 0);
	tail->position(3, 1, -0.4f);

	tail->getRotationAnimation()->addAnimationFrame(0, 0, 0);
	tail->getRotationAnimation()->addAnimationFrame(0, 0, -30);
	tail->setRotationOrigin(0.0f, 0.0f, 0.0f);

	DisplayTreeNode* headNode = new DisplayTreeNode(head);
	DisplayTreeNode* bodyNode = new DisplayTreeNode(body);
	DisplayTreeNode* tailNode = new DisplayTreeNode(tail);

	headNode->addChild(bodyNode);
	bodyNode->addChild(tailNode);

	this->setParentDisplayTreeNode(headNode);
}

Dog::~Dog()
{
}
