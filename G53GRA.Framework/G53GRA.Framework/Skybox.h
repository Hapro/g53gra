#pragma once
#include "DisplayableObject.h"

class Skybox : public DisplayableObject
{
public:
	Skybox(Material material, std::map<std::string, int> textures);
	~Skybox();

	void drawTextureMaterial() override;
	void drawMaterial() override {};
	
private:
	const int SIZE = 300;
};

