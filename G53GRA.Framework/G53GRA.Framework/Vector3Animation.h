#pragma once
#include "Animation.h"

class Vector3Animation
{
public:
	Vector3Animation(Animation* xAnimation,
		Animation* yAnimation,
		Animation* zAnimation) 
	{
		m_xAnimation = xAnimation;
		m_yAnimation = yAnimation;
		m_zAnimation = zAnimation;
	}

	Vector3Animation()
	{
		m_xAnimation = new Animation(0);
		m_yAnimation = new Animation(0);
		m_zAnimation = new Animation(0);
	}

	~Vector3Animation() {}

	Animation* getXAnimation() { return m_xAnimation; }
	Animation* getYAnimation() { return m_yAnimation; }
	Animation* getZAnimation() { return m_zAnimation; }

	void addAnimationFrame(float x, float y, float z){
		m_xAnimation->addFrame(new AnimationFrame(x));
		m_yAnimation->addFrame(new AnimationFrame(y));
		m_zAnimation->addFrame(new AnimationFrame(z));
	}

	void update(float deltaTimeInSeconds) {
		m_xAnimation->update(deltaTimeInSeconds);
		m_yAnimation->update(deltaTimeInSeconds);
		m_zAnimation->update(deltaTimeInSeconds);
	}

private:
	Animation* m_xAnimation;
	Animation* m_yAnimation;
	Animation* m_zAnimation;
};

