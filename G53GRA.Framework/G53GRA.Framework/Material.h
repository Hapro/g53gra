#pragma once
#include "Vector4.h"
#include "Colour.h"

class Material
{
public:
	Material();
	Material(Vector4 specular, Vector4 ambient, Vector4 diffuse, float shine);
	~Material();

	Vector4 getSpecular();
	Vector4 getAmbient();
	Vector4 getDiffuse();
	float getShine();

private:
	Vector4 m_specular;
	Vector4 m_ambient;
	Vector4 m_diffuse;
	float m_shine;
};

static class Materials{
public:
	static Material shiny(Colour colour){
		return Material(
			Vector4(1.0f, 1.0f, 1.0f, 1.0f),
			Vector4(0.0f, 0.0f, 0.0f, 0.0f),
			Vector4(colour.red(), colour.green(), colour.blue(), 1.0f),
			64.0f);
	}
};

