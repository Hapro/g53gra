#pragma once

class Colour
{
public:
	Colour();
	Colour(float red, float green, float blue);
	~Colour();

	float red();
	float green();
	float blue();

private:
	float m_red;
	float m_green;
	float m_blue;
};

static class Colours{
public:
	static Colour Red(){ return Colour(1.0f, 0.0f, 0.0f); }
	static Colour Green(){ return Colour(0.0f, 1.0f, 0.0f); }
	static Colour Blue(){ return Colour(0.0f, 0.0f, 1.0f); }
	static Colour Pink(){ return Colour(0.964f, 0.435f, 0.941f); }
	static Colour Teal(){ return Colour(0.435f, 0.862f, 0.964f); }
	static Colour Brown(){ return Colour(0.513f, 0.313f, 0.145f); }
	static Colour White(){ return Colour(1.0f, 1.0f, 1.0f); }
	static Colour Black(){ return Colour(0.0f, 0.0f, 0.0f); }
};
