#include "Light.h"

#ifdef _WIN32
#include <GL/glut.h>
#include <Windows.h>
#else
#include <GLUT/glut.h>
#endif

Light::Light(Material material, long glLightNumber, float x, float y, float z)
{
	m_material = material;
	m_position = Vector4(x, y, z, 1.0f);
	m_glLightNumber = glLightNumber;
}

Light::~Light()
{
}

void Light::setX(float x)
{
	m_position = Vector4(x, m_position.getY(), m_position.getZ(), m_position.getA());
	update();
}

void Light::setY(float y)
{
	m_position = Vector4(m_position.getX(), y, m_position.getZ(), m_position.getA());
	update();
}

void Light::setZ(float z)
{
	m_position = Vector4(m_position.getX(), m_position.getY(), z, m_position.getA());
	update();
}

Vector4 Light::getPosition()
{
	return m_position;
}

void Light::update()
{
	float ambient[4] = {
		m_material.getAmbient().getX(),
		m_material.getAmbient().getY(),
		m_material.getAmbient().getZ(),
		m_material.getAmbient().getA()
	};

	float diffuse[4] = {
		m_material.getDiffuse().getX(),
		m_material.getDiffuse().getY(),
		m_material.getDiffuse().getZ(),
		m_material.getDiffuse().getA()
	};

	float specular[4] = {
		m_material.getSpecular().getX(),
		m_material.getSpecular().getY(),
		m_material.getSpecular().getZ(),
		m_material.getSpecular().getA()
	};
	
	float position[4] = {
		m_position.getX(),
		m_position.getY(),
		m_position.getZ(),
		m_position.getA()
	};

	glLightfv(m_glLightNumber, GL_AMBIENT, ambient);
	glLightfv(m_glLightNumber, GL_DIFFUSE, diffuse);
	glLightfv(m_glLightNumber, GL_SPECULAR, specular);
	glLightfv(m_glLightNumber, GL_POSITION, position);

	glEnable(m_glLightNumber);
}
