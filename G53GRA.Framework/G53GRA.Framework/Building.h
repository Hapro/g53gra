#pragma once
#include "HierarchalModel.h"

class Colour;
class Building : public HierarchalModel
{
public:
	Building(Colour colour, int stories);
	~Building();

private:
	int const SIZE = 10;
	int const WINDOW_SIZE = 3;
};

