#include "Material.h"

Material::Material()
{
	m_specular = Vector4();
	m_ambient = Vector4();
	m_diffuse = Vector4();
}

Material::Material(Vector4 specular, Vector4 ambient, Vector4 diffuse, float shine)
{
	m_specular = specular;
	m_ambient = ambient;
	m_diffuse = diffuse;
	m_shine = shine;
}

Material::~Material()
{
}

float Material::getShine(){
	return m_shine;
}

Vector4 Material::getSpecular()
{
	return m_specular;
}

Vector4 Material::getAmbient()
{
	return m_ambient;
}

Vector4 Material::getDiffuse()
{
	return m_diffuse;
}
